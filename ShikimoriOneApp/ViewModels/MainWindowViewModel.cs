using ReactiveUI;
using ShikimoriOneApp.ViewModels.Filters;
using ShikimoriSharp.Classes;
using System.Diagnostics;

namespace ShikimoriOneApp.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public Filters.RanobeFiltersViewModel? RanobeFiltersViewModel
        {
            get => _RanobeFiltersViewModel;
            set => _RanobeFiltersViewModel = value;
        }
        public Filters.AnimeFiltersViewModel? AnimeFiltersViewModel
        {
            get => _AnimeFiltersViewModel;
            set => _AnimeFiltersViewModel = value;
        }
        public Filters.MangaFiltersViewModel? MangaFiltersViewModel
        {
            get => _MangaFiltersViewModel;
            set => _MangaFiltersViewModel = value;
        }
        public bool ArticleIsVisible
        {
            get => _ArticleIsVisible;
            set => this.RaiseAndSetIfChanged(ref _ArticleIsVisible, value);
        }
        public ArticleViewModel? ArticleContext
        {
            get => _ArticleContext;
            set => this.RaiseAndSetIfChanged(ref _ArticleContext, value);
        }
        public object? FilterMenuContext
        {
            get => _FilterMenuContext;
            set => this.RaiseAndSetIfChanged(ref _FilterMenuContext, value);
        }
        public object? Dcontext
        {
            get => _Dcontext;
            set => this.RaiseAndSetIfChanged(ref _Dcontext, value);
        }
        public int MainMenuSelectedIndex {
            get => _MainSelectedIndex;
            set {
                MangaFiltersViewModel?.ToggleEvents(false);
                AnimeFiltersViewModel?.ToggleEvents(false);
                RanobeFiltersViewModel?.ToggleEvents(false);
                switch (value) {
                    case 0: {
                        FilterMenuContext = MangaFiltersViewModel;
                        Dcontext = _MangaListControlViewModel;

                        MangaFiltersViewModel?.ToggleEvents(true);
                        break;
                    } case 1: {
                        FilterMenuContext = AnimeFiltersViewModel;
                        Dcontext = _AnimeListControlViewModel;

                        AnimeFiltersViewModel?.ToggleEvents(true);
                        break;
                    } case 2: {
                        FilterMenuContext = RanobeFiltersViewModel;
                        Dcontext = _RanobeListControlViewModel;

                        RanobeFiltersViewModel?.ToggleEvents(true);
                        break;
                    }
                }
                _MainSelectedIndex = value;
            }
        }
        private bool _ArticleIsVisible;
        private ArticleViewModel? _ArticleContext;
        private AnimeFiltersViewModel? _AnimeFiltersViewModel;
        private MangaFiltersViewModel? _MangaFiltersViewModel;
        private RanobeFiltersViewModel? _RanobeFiltersViewModel;
        private AnimeListControlViewModel? _AnimeListControlViewModel;
        private MangaListControlViewModel? _MangaListControlViewModel;
        private RanobeListControlViewModel? _RanobeListControlViewModel;
        private object? _Dcontext;
        private object? _FilterMenuContext;
        private int _MainSelectedIndex = 1;

        public MainWindowViewModel() {
            _RanobeFiltersViewModel = new();
            _MangaFiltersViewModel = new();
            _AnimeFiltersViewModel = new();

            _AnimeListControlViewModel = new(_AnimeFiltersViewModel);
            _MangaListControlViewModel = new(_MangaFiltersViewModel);
            _RanobeListControlViewModel = new(_RanobeFiltersViewModel);

            FilterMenuContext = AnimeFiltersViewModel;

            Dcontext = _AnimeListControlViewModel;

            ExtensionMethods.OpenArticleEvent += OpenArticleEventHandler;
        }

        public void OpenArticleEventHandler(ArticleViewModel? Model)
        {
            if (Model is not null)
            {
                ArticleContext = Model;
                Model.CloseArticleEvent += CloseArticleEventHandler;
                Model.OpenOtherArticleEvent += OpenOtherArticleEventHandler;
                ArticleIsVisible = true;
            }
            else
            {
                ArticleIsVisible = false;
                Debug.WriteLine("Article Model Is NULL");
            }
        }

        private void CloseArticleEventHandler()
        {
            if (ArticleContext is not null)
                ArticleContext.CloseArticleEvent -= CloseArticleEventHandler;

            ArticleContext = null;
            ArticleIsVisible = false;
        }

        private async void OpenOtherArticleEventHandler(long id) {
            var apiClient = ShikikomoriApiHandler.ApiClient ?? throw new System.NullReferenceException();
            ArticleViewModel articleViewModel;
            if (ArticleContext?.Article is AnimeID) {
                articleViewModel = new ArticleViewModel(await apiClient.Animes.GetAnime(id));
            } else if (ArticleContext?.Article is MangaID) {
                articleViewModel = new ArticleViewModel(await apiClient.Mangas.GetById(id));
            } else {
                return;
            }

            CloseArticleEventHandler();
            OpenArticleEventHandler(articleViewModel);
        }
    }
}