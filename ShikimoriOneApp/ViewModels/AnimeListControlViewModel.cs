﻿
using ReactiveUI;
using ShikimoriOneApp.Filters;
using ShikimoriOneApp.ViewModels.Filters;
using ShikimoriSharp.AdditionalRequests;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ShikimoriOneApp.ViewModels
{
    public class AnimeListControlViewModel : ViewModelBase
    {

        public Filters.AnimeFiltersViewModel? Filters {
            get => _FiltersViewModel;
            set => _FiltersViewModel = value;
        }
        public ObservableCollection<Models.DataModel> DataCollection 
            { get; set; } = new ObservableCollection<Models.DataModel>();
        public ArticleViewModel? ArticleViewModel {
            get => _articleViewModel;
            set => this.RaiseAndSetIfChanged(ref _articleViewModel, value);
        }
        public bool ArticleControlIsVisible {
            get => _articleControlIsVisible;
            set => this.RaiseAndSetIfChanged(ref _articleControlIsVisible, value);
        }
        public Models.DataModel? SelectedModel {
            get => _selectedModel;
            set {
                _selectedModel = value;
                if (_selectedModel is not null) {
                    Task.Run(async () => {
                        try {
                            if (ShikikomoriApiHandler.ApiClient is not null 
                                && _selectedModel.Id is not null) {

                                int animeId = (int)_selectedModel.Id;
                                var anime = await ShikikomoriApiHandler.ApiClient.Animes.GetAnime(animeId);
                                ExtensionMethods.CallOpenArticleEvent(new ArticleViewModel(anime));
                            }

                        } catch (Exception ex) {
                            Debug.WriteLine(ex);
                        }
                    });
                }
            }
        }
        public bool IsLoadingPanelVisible { 
            get => _isLoadingPanelVisible; 
            set => this.RaiseAndSetIfChanged(ref _isLoadingPanelVisible, value); 
        }
        public IReactiveCommand LoadNextPageCommand { get; set; }
        public IReactiveCommand LoadPreviousPageCommand { get; set; }
        private Filters.AnimeFiltersViewModel? _FiltersViewModel;
        private ArticleViewModel? _articleViewModel;
        private bool _articleControlIsVisible = false;
        private int _page = 1;
        private bool _isLoadingPanelVisible;
        private Models.DataModel? _selectedModel;

        public AnimeListControlViewModel(AnimeFiltersViewModel filters)
        {

            Filters = filters;
            Load();
            LoadPreviousPageCommand = ReactiveCommand.Create(() =>
            {
                if (_page <= 1)
                    return;

                _page += -1;
                Load();
            });
            LoadNextPageCommand = ReactiveCommand.Create(() =>
            {
                _page += 1;
                Load();
            });
            Filters.FilterChanged += delegate () { _page = 1; Load(); };
        }

        public void Load() {
            IsLoadingPanelVisible = true;
            DataCollection.Clear();
            Task.Run(async () => {
                try {
                    if (ShikikomoriApiHandler.ApiClient is not null && Filters is not null) {
                        var res = await ShikikomoriApiHandler.ApiClient.Animes.GetAnime(
                            Filters.GetRequestSettings(_page));
                        // foreach(ShikimoriSharp.Classes.Anime anime in res) {
                        //     long animeId = anime.Id.Value;
                        //     var franchise = await ShikikomoriApiHandler.ApiClient.Animes.GetFranchise(animeId);
                        //     Console.WriteLine(franchise.Nodes[0].Name);
                        // }
                        DataCollection.AddRange(res);
                    }
                }
                catch (Exception ex) { Debug.WriteLine(ex); }

                IsLoadingPanelVisible = false;
            });
        }
    }
}
