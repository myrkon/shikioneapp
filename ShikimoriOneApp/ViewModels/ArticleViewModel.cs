﻿using ReactiveUI;
using ShikimoriOneApp.Models;
using ShikimoriSharp.Classes;
using ShikimoriSharp.AdditionalRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace ShikimoriOneApp.ViewModels {
    public class ArticleViewModel : ViewModelBase {
        public delegate void CloseActicle();
        public event CloseActicle? CloseArticleEvent;

        public delegate void OpenOtherArticle(long id);
        public event OpenOtherArticle? OpenOtherArticleEvent;

        public ImageModel? Poster {
            get {
                if (_Poster is null) {
                    string? url = null;
                    if (Article is AnimeID) {
                        var anime = Article as AnimeID;
                        url = anime?.Image?.Original;
                    }

                    if (Article is MangaID) {
                        var manga = Article as MangaID;
                        url = manga?.Image?.Original;
                    }

                    if (url != null) {
                        _Poster = new ImageModel() {
                            Url = "https://shikimori.one/" + url,
                        };
                    }
                }

                return _Poster;
            }
        }
        public bool DescriptionIsVisible {
            get {
                if (Article is AnimeID) {
                    var anime = Article as AnimeID;
                    return anime?.Description is not null;
                }
                if (Article is MangaID) {
                    var manga = Article as MangaID;
                    return manga?.Description is not null;
                }
                return false;
            }
        }
        public bool OtherTitlesIsVisible { get => (OtherTitles?.Count > 0); }
        public bool ScreeenShotsIsVisible { get => (ScreenShots?.Count > 0); }
        public bool VideosIsVisible { get => Videos?.Count > 0; }
        public List<string>? OtherTitles {
            get {
                if (_OtherTitles is null) {
                    _OtherTitles = new List<string>();
                    if (Article is AnimeID) {
                        var anime = Article as AnimeID;
                        if (anime?.Russian is not null && anime.Russian?.Length > 0) {
                            _OtherTitles?.Add(anime.Russian);
                        }
                        if (anime?.English is not null) {
                            _OtherTitles?.AddRange(anime.English.Where(x => x is not null));
                        }
                        if (anime?.Japanese is not null) {
                            _OtherTitles?.AddRange(anime.Japanese.Where(x => x is not null));
                        }
                        if (anime?.Synonyms is not null) {
                            _OtherTitles?.AddRange(anime.Synonyms.Where(x => x is not null));
                        }
                    }

                    if (Article is MangaID) {
                        var manga = Article as MangaID;
                        if (manga?.Russian is not null && manga.Russian?.Length > 0) {
                            _OtherTitles?.Add(manga.Russian);
                        }
                        if (manga?.English is not null) {
                            _OtherTitles?.AddRange(manga.English.Where(x => x is not null));
                        }
                        if (manga?.Japanese is not null) {
                            _OtherTitles?.AddRange(manga.Japanese.Where(x => x is not null));
                        }
                        if (manga?.Synonyms is not null) {
                            _OtherTitles?.AddRange(manga.Synonyms.Where(x => x is not null));
                        }
                    }
                }
                return _OtherTitles;
            }
        }
        public List<ImageModel> Videos {
            get {
                if (_Videos is null) {
                    _Videos = new List<ImageModel>();
                    if (Article is AnimeID) {
                        var anime = Article as AnimeID;
                        if (anime?.Videos is not null) {
                            foreach (var video in anime.Videos) {
                                _Videos.Add(new ImageModel() { Url = video.ImageUrl.AbsoluteUri });
                            }
                        }
                    }

                }
                return _Videos;
            }
        }
        public List<ImageModel> ScreenShots {
            get {
                if (_ScreenShots is null) {
                    _ScreenShots = new List<ImageModel>();
                    if (Article is AnimeID) {
                        var anime = Article as AnimeID;

                        if (anime is not null && anime.Screens is not null) {
                            foreach (var screenshot in anime.Screens) {
                                _ScreenShots.Add(new ImageModel() { Url = "https://shikimori.one/" 
                                    + screenshot.Preview });
                            }
                        }
                    }

                }
                return _ScreenShots;
            }
        }
        public string? ArticleTitle {
            get {
                if (Article is AnimeID) {
                    var anime = Article as AnimeID;

                    return string.Format("{0} / {1}", anime?.Name, anime?.Russian);
                }

                if (Article is MangaID) {
                    var manga = Article as MangaID;
                    return string.Format("{0} / {1}", manga?.Name, manga?.Russian);
                }
                return "";
            }
        }

        public string? Title { 
            get { 
                if (Article is AnimeID) {
                    var articleObj = Article as AnimeID;
                    return $"Название {articleObj?.Name}"; 
                }
                if (Article is MangaID) {
                    var articleObj = Article as AnimeID;
                    return $"Название {articleObj?.Name}"; 
                }
                return "";
            } 
        }
        public object? Article { get; set; }
        public IReactiveCommand? CloseCommand { get; set; }
        public IReactiveCommand? OpenOtherArticleCommand { get; set; }
        public ObservableCollection<string> Franchise {
            get => _franchise;
            set => this.RaiseAndSetIfChanged(ref _franchise, value);
        }
        public int FranchiseSelectedIndex {
            get => _franchiseSelectedIndex;
            set => this.RaiseAndSetIfChanged(ref _franchiseSelectedIndex, value);
        }
        private int _franchiseSelectedIndex;
        private List<long> FranchiseIds;
        private ObservableCollection<string> _franchise;
        private List<string>? _OtherTitles;
        private List<ImageModel>? _ScreenShots;
        private List<ImageModel>? _Videos;
        private ImageModel? _Poster;

        public void OpenOtherArticleShit() => OpenOtherArticleEvent?.Invoke(FranchiseIds[FranchiseSelectedIndex]);

        public void Init() {
            CloseCommand = ReactiveCommand.Create(() => CloseArticleEvent?.Invoke());
            OpenOtherArticleCommand = ReactiveCommand.Create((Avalonia.Input.TappedEventArgs _) => 
                OpenOtherArticleEvent?.Invoke(FranchiseIds[FranchiseSelectedIndex]));

            long id;
            if (Article is AnimeID) {
                var articleObj = Article as AnimeID;
                id = articleObj?.Id ?? -1;
            } else if (Article is MangaID) {
                var articleObj = Article as MangaID;
                id = articleObj?.Id ?? -1;
            } else {
                id = -1;
            }

            if (id != -1) {
                Task.Run(async () => {
                    var apiClient = ShikikomoriApiHandler.ApiClient ?? 
                                    throw new NullReferenceException();
                    Franchise franchise;
                    if (Article is AnimeID) {
                        franchise = await apiClient.Animes.GetFranchise(id);
                    } else if (Article is MangaID) {
                        franchise = await apiClient.Mangas.GetFranchise(id);
                    } else {
                        franchise = await apiClient.Ranobe.GetFranchise(id);
                    }
                    var nodes = franchise.Nodes ?? throw new NullReferenceException("franchise is null");
                    foreach(Node node in nodes) {
                        Franchise.Add($"{node.Name}");
                        FranchiseIds.Add(node.Id);
                    }
                });
            }
        }

        public ArticleViewModel(AnimeID animeID) {
            _franchise = new ObservableCollection<string>();
            FranchiseIds = new List<long>();
            Article = animeID;
            Init();
        }

        public ArticleViewModel(MangaID mangaID) {
            _franchise = new ObservableCollection<string>();
            FranchiseIds = new List<long>();
            Article = mangaID;
            Init();
        }
    }
}